package cat.itb.albumfotos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.albumfotos.camerascreen.PhotoDisplayFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, PhotoDisplayFragment.newInstance())
                    .commitNow();
        }

    }
}
