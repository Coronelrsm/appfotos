package cat.itb.albumfotos.camerascreen;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.albumfotos.R;

import static android.app.Activity.RESULT_OK;

public class PhotoDisplayFragment extends Fragment {

    @BindView(R.id.picture)
    ImageView picture;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.fabShare)
    FloatingActionButton fabShare;
    private PhotoDisplayViewModel mViewModel;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    public static PhotoDisplayFragment newInstance() {
        return new PhotoDisplayFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.photo_display_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PhotoDisplayViewModel.class);
        //Glide.with(this).load("http://goo.gl/gEgYUd").apply(RequestOptions.circleCropTransform()).into(picture);


    }


    /*

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(this.getContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

     */

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(this.getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this.getContext(),
                        "cat.itb.fileprovider",
                        photoFile);

                ///home/sjo/Escriptori/AppFotos/app/src/main/res/raw

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Glide.with(this).load(currentPhotoPath).apply(RequestOptions.circleCropTransform()).into(picture);

            //Bundle extras = data.getExtras();  //peta aquí???? ------------ extras és null ---------------//
            //Bitmap imageBitmap = (Bitmap) extras.get("data");         // això fallava perquè ha de seguir la ruta de la Uri. Ha de fer un load(photoPath) que és la Uri en el mètode dispatchTakePictureIntent()
            //picture.setImageBitmap(imageBitmap);

        } else {
            Toast toast = Toast.makeText(getContext(), "Foto Fallida!!!! ", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @OnClick({R.id.fab, R.id.fabShare})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab:
                dispatchTakePictureIntent();
                break;


            case R.id.fabShare:
                sharePhoto();
                break;
        }
    }

    private void sharePhoto() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpeg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, currentPhotoPath);
        startActivity(Intent.createChooser(shareIntent, "Share This Epic Photo" ));
    }


    //
}
